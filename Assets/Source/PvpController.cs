﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Colyseus;
using Colyseus.Schema;
using GameDevWare.Serialization;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PvpController : MonoBehaviour {
	[HideInInspector] private const bool TEST_PLAY_WITH_BOT = false;

	static readonly string HOST = "ec2-3-1-100-94.ap-southeast-1.compute.amazonaws.com";
	static readonly string PORT = "2567";
	static readonly string EndPoint = "ws://" + HOST + ":" + PORT;
	static readonly string RoomName = "pvp";

	private Room<PvpState> room;
	private Client client;
	public RoomState CurrentState;
	public PvpComponent PvpComponent = new PvpComponent();
	private ConnectionErrorType userConnectionErrorType = ConnectionErrorType.NORMAL;
	public Button ButtonFindMatch;
	public Button ButtonLeaveRoom;
	// Start is called before the first frame update
	void Start() {
		PvpComponent.isPlaying = false;
		PvpComponent.isActive = true;
		ButtonFindMatch.onClick.AddListener(()=>StartFindMatch(10));
		ButtonLeaveRoom.onClick.AddListener(() => {
			if (room != null && room.Connection != null && room.Connection.IsOpen)
			{
				room.Leave(false);
				if (!PvpComponent.isPlayingWithBot) room = null;
			}
		});
	}

	// Update is called once per frame
	void Update() {
		if (client != null) {
			try {
				client.Recv();
			}
			catch (Exception e) {
				if (e.Message.Contains("Local schema mismatch from server")) {
//                    userConnectionErrorType = ConnectionErrorType.SCHEMA_TYPE_MISMATCH;
				}

				if (e.Message.Contains("out of sync")) {
					print("Data out of sync \n Some unknown error, maybe harmless??");
				}
				else Debug.LogError(e.Message + "\n" + e.StackTrace);
			}
		}

		if (userConnectionErrorType != ConnectionErrorType.NORMAL) {
			// ShowDisconnectToast(userConnectionErrorType);
			userConnectionErrorType = ConnectionErrorType.NORMAL;
		}

		if (CurrentStateStartTime != default(DateTime) && room != null) {
			TimeSpan remainTime;
			switch (CurrentState) {
				case RoomState.WAITING_FOR_PLAYER:
					break;
				case RoomState.WAITING_FOR_MATCH_START:
					remainTime = TimeSpan.FromMilliseconds(room.State.startDuration) -
					             (DateTime.Now - CurrentStateStartTime);
//                    PvPMatching.SetCountdownTime(remainTime, room.State.startDuration);
					if (remainTime.TotalMilliseconds < 0 && PvpComponent.isPlayingWithBot) {
						BotMatchSetRoomState(RoomState.PLAYING);
					}

					break;
				case RoomState.PLAYING:
					remainTime = TimeSpan.FromMilliseconds(room.State.duration) -
					             (DateTime.Now - CurrentStateStartTime);
//                    PvpBreakerInGamePanel.TimeProgress.fillAmount =
//                        (float) remainTime.TotalMilliseconds / room.State.duration;
//                    PvpBreakerWaitDeadPanel.TimeProgress.fillAmount = PvpBreakerInGamePanel.TimeProgress.fillAmount;

					if (remainTime.TotalMilliseconds < 0 && PvpComponent.isPlayingWithBot) {
						var myPlayer = PvpComponent.myPlayer;
						var opponentPlayer = PvpComponent.opponentPlayer;
//                        SetBotMatchWinner(myPlayer.point > opponentPlayer.point ? myPlayer : opponentPlayer);
					}

					break;
				case RoomState.GAME_OVER:
					break;
			}
		}
	}

	void StartJoinRoom() {
		OnRoomStateChange(RoomState.WAITING_FOR_PLAYER);
		StartCoroutine(JoinRoom());
	}


	void OnRoomStateChange(RoomState roomState) {
		CurrentStateStartTime = DateTime.Now;
		switch (roomState) {
			case RoomState.WAITING_FOR_PLAYER:

				break;
			case RoomState.WAITING_FOR_MATCH_START:

				break;
			case RoomState.PLAYING:

				break;
			case RoomState.GAME_OVER:
				break;
		}
	}

	public DateTime CurrentStateStartTime { get; set; }

	IEnumerator JoinRoom() {
		yield return new WaitForSeconds(0);
		//if (PvpComponent.userForceDisconnect) yield break; khi user force disconnect

		bool isLogin = true;
		var userName = isLogin ? "Tund" : "TestName";
		var country = isLogin ? "us" : "vn";
		var betValue = 69; // gia tri BET
		int streak = 1; // so lan thang lien tiep
		var botProfit = 69; //so tien thang dc tu boss

		var curPoint = 6969; // diem pvp

		var joinParam = new Dictionary<string, object> {
			//{"level", botProfit > 200 ? m_ListLevels.GetRandomIdHard() : m_ListLevels.GetRandomId()},
			{"level", 32},
			{"battlePoint", curPoint},
			{"name", userName},
			{"moneyBet", betValue},
			{"country", country},
			{"streak", streak},
			{"win", 2},
            {"lose", 2},
            {"pvpRank", 2},
            {"pvpTitle", 1},
            {"ship", ""},
            {"sub", ""},
            {"core", ""},
		};
		room = client.Join<PvpState>(RoomName, joinParam);

		room.OnReadyToConnect += (sender, e) => { StartCoroutine(room.Connect()); };
		room.OnError += (sender, e) => { Debug.Log(e.Message); };
		room.OnJoin += (sender, e) => {
			Debug.Log("Joined room successfully sessionId: " + room.SessionId);
//                PvpComponent.leaveRoomOnFindMatch = false;

			room.State.players.OnAdd += OnPlayerAdd;
			room.State.players.OnRemove += OnPlayerRemove;
			room.State.players.OnChange += OnPlayerChange;

//                if (findMatchType == FindMatchType.NORMAL && (TEST_PLAY_WITH_BOT || betValue == 200))
//                    //if (TEST_PLAY_WITH_BOT)
//                {
//                    StartCoroutine(StartBotMatch(1f));
//                    StartCoroutine(IELeaveRoom(1f));
//                }
		};
		room.OnLeave += (sender, e) => {
			if (!PvpComponent.isPlayingWithBot && !PvpComponent.userForceDisconnect) {
				if (!PvpComponent.leaveRoomOnFindMatch) {
//                    if (room.State.roomState != (short) RoomState.GAME_OVER) 
					// userConnectionErrorType = ConnectionErrorType.DISCONNECT;
				}
				else room = null;
			}
		};

		room.OnStateChange += OnStateChangeHandler;
		room.OnMessage += OnMessage;
	}

	void OnPlayerAdd(object sender, KeyValueEventArgs<PvpPlayer, string> item) {
		// add "player" to map of players
		var player = item.Value;
		if (player.id == room.SessionId) {
//                PvpComponent.myPlayer = PvPResult.MyPlayer.player = player;
//                PvpBreakerInGamePanel.Player1NameText.text = PvpBreakerWaitDeadPanel.Player1NameText.text = player.name;
//                PvPMatching.MyPlayer.Name.text = player.name;
//                PvPMatching.MyPlayer.PvpPoint.gameObject.SetActive(true);
//                PvPMatching.MyPlayer.PvpPoint.text = "Pvp Point: " + player.battlePoint;
//                PvPMatching.MyPlayer.Flag.overrideSprite = m_ListLevels.GetFlagRect(player.country);
//
//                PvPResult.MyPlayer.TextName.text = player.name;
//                PvpBreakerInGamePanel.Player1Flag.overrideSprite = PvpBreakerWaitDeadPanel.Player1Flag.overrideSprite =
//                    m_ListLevels.GetFlagCircle(player.country);
//                PvPResult.MyPlayer.Flag.overrideSprite = m_ListLevels.GetFlagCircle(player.country);
//                PvPResult.MyPlayer.TextRewardLose.text = "0";
		}
		else {
//                StopFindMatchEffect();
//                PvpComponent.opponentPlayer = PvPResult.OpponentPlayer.player = player;
//                PvpBreakerInGamePanel.Player2NameText.text =
//                    PvpBreakerWaitDeadPanel.Player2NameText.text = LiveStreamName.text = player.name;
//                PvPMatching.OpponentPlayer.Name.text = player.name;
//                PvPMatching.OpponentPlayer.PvpPoint.gameObject.SetActive(true);
//                PvPMatching.OpponentPlayer.PvpPoint.text = "Pvp Point: " + player.battlePoint;
//                PvPMatching.OpponentPlayer.Flag.overrideSprite = m_ListLevels.GetFlagRect(player.country);
//
//                PvPResult.OpponentPlayer.TextName.text = player.name;
//                PvpBreakerInGamePanel.Player2Flag.overrideSprite = PvpBreakerWaitDeadPanel.Player2Flag.overrideSprite =
//                    m_ListLevels.GetFlagCircle(player.country);
//                PvPResult.OpponentPlayer.Flag.overrideSprite = m_ListLevels.GetFlagCircle(player.country);
//                PvPResult.OpponentPlayer.TextRewardLose.text = "0";
		}

		OnPlayerChange(sender, item);
	}

	void OnPlayerRemove(object sender, KeyValueEventArgs<PvpPlayer, string> item) {
	}

	void OnPlayerChange(object sender, KeyValueEventArgs<PvpPlayer, string> item) {
		var player = item.Value;
		var pointText = player.point + "%";
		if (player.id == room.SessionId) {
//            PvpBreakerInGamePanel.Player1PointText.text
//                = PvpBreakerWaitDeadPanel.Player1PointText.text
//                    = PvPResult.MyPlayer.TextCompleteWin.text
//                        = PvPResult.MyPlayer.TextCompleteLose.text = pointText;
		}
		else {
//            PvpBreakerInGamePanel.Player2PointText.text
//                = PvpBreakerWaitDeadPanel.Player2PointText.text
//                    = PvPResult.OpponentPlayer.TextCompleteWin.text
//                        = PvPResult.OpponentPlayer.TextCompleteLose.text = pointText;

			if (player.isDead) {
//                Components.LevelPvp.isGameOver = true;
			}
		}

		if (PvpComponent.myPlayer != null && PvpComponent.opponentPlayer != null) {
//            var colorWin = ColorHelper.GetColorFromHex("#6BF54C");
//            var colorLose = ColorHelper.GetColorFromHex("#FF5656");
//
//            PvpBreakerInGamePanel.Player1PointText.color
//                = PvpBreakerWaitDeadPanel.Player1PointText.color
//                    = PvpBreakerInGamePanel.Player2PointText.color
//                        = PvpBreakerWaitDeadPanel.Player2PointText.color
//                            = colorWin;
//            if (PvpComponent.myPlayer.point > PvpComponent.opponentPlayer.point)
//            {
//                PvpBreakerInGamePanel.Player2PointText.color
//                    = PvpBreakerWaitDeadPanel.Player2PointText.color
//                        = colorLose;
//            }
//            else if (PvpComponent.myPlayer.point < PvpComponent.opponentPlayer.point)
//            {
//                PvpBreakerInGamePanel.Player1PointText.color
//                    = PvpBreakerWaitDeadPanel.Player1PointText.color
//                        = colorLose;
//            }
		}
	}

	void OnApplicationQuit() {
		// Make sure client will disconnect from the server
		if (room != null) {
			room.Leave();
		}

		if (client != null) {
			client.Close();
		}
	}

	void OnStateChangeHandler(object sender, StateChangeEventArgs<PvpState> e) {
		var state = (RoomState) e.State.roomState;
		if (CurrentState != state) {
			OnRoomStateChange(state);
		}
	}

	void OnMessage(object sender, MessageEventArgs e) {
		var message = (IndexedDictionary<string, object>) e.Message;
		print("Receive Server Message: " + JsonConvert.SerializeObject(message));

		if (message.ContainsKey("increaseCondition") && (bool) message["increaseCondition"]) {
			PvpComponent.conditionValue++;
			PvpComponent.leaveRoomOnFindMatch = true;

			{
				// TODO: Start playing with BOT
				print("NO MATCH FOUND! START PLAYING WITH BOT!");
				StartCoroutine(StartBotMatch());
			}
		}
		else if (message.ContainsKey("shotBall")) {
//                var level = Components.LevelPvp;
//                level.shotCount += 1;
//
//                if (level.shotCount > 1)
//                {
//                    var data = PvpComponent.opponentPlayer.playingMap;
//
//                    var bricksInfo = level.streamBricksInfo;
//                    int count = Mathf.Min(data.Count, bricksInfo.Count);
//                    for (int i = 0; i < count; i++)
//                    {
//                        var info = bricksInfo[i];
//                        info.hp = data[i];
//                        bricksInfo[i] = info;
//                    }
//
//                    m_StreamLevel.SetBricksInfo();
//                }
//
//                var posX = float.Parse(message["posX"].ToString());
//                var startPosX = posX * level.playSizeX;
//                m_StreamBall.StartRunBalls(int.Parse(message["ball"].ToString()),
//                    new Vector3(float.Parse(message["directX"].ToString()), float.Parse(message["directY"].ToString())),
//                    new Vector3(startPosX, level.startPosY));
		}
		else if (message.ContainsKey("stopBall")) {
//                int timeTick = int.Parse(message["timeTick"].ToString());
//                m_StreamBall.MoveAllBallBack(timeTick);
		}
	}

	IEnumerator StartBotMatch(float waitTime = 0) {
		yield return new WaitForSeconds(waitTime);

		var myPlayer = PvpComponent.myPlayer;
		if (myPlayer == null) {
//            userConnectionErrorType = ConnectionErrorType.DISCONNECT;
			yield break;
		}

		PvpComponent.isPlayingWithBot = true;
		var botPlayer = new PvpPlayer {
//            id = "it's a fucking bot",
//            country = m_ListLevels.GetRandomCountry(),
//            name = m_ListLevels.GetRandomBotName(),
//            battlePoint = (short) (myPlayer.battlePoint + Random.Range(-80, 80)),
//            streak = 0
		};
		CalculateBattlePoint(myPlayer, botPlayer);
		CalculateBattlePoint(botPlayer, myPlayer);

		room.State.players.Add("bot", botPlayer);
		room.State.players.InvokeOnAdd(botPlayer, botPlayer.id);
		room.State.players.InvokeOnChange(myPlayer, myPlayer.id);
		BotMatchSetRoomState(RoomState.WAITING_FOR_MATCH_START);
	}

	void CalculateBattlePoint(PvpPlayer player1, PvpPlayer player2) {
//        var pointDiff = player1.battlePoint - player2.battlePoint;
//        var pointDiffAbs = Mathf.Abs(pointDiff);
//        var pointVariation = Mathf.Min(pointDiffAbs / 50 << 0, 2);
//        if (pointDiff >= 0)
//        {
//            player1.battlePointWin = (short) (player1.battlePoint + 23 + pointVariation);
//            player1.battlePointLose = (short) (player1.battlePoint - 15 + pointVariation);
//        }
//        else
//        {
//            player1.battlePointWin = (short) (player1.battlePoint + 22 - pointVariation);
//            player1.battlePointLose = (short) (player1.battlePoint - 16 - pointVariation);
//        }
//
//        player1.battlePointLose = (short) Mathf.Max(player1.battlePointLose, 0);
	}


	void BotMatchSetRoomState(RoomState roomState) {
		OnRoomStateChange(roomState);
	}

	public void StartFindMatch(int betId) {
		PvpComponent.userForceDisconnect = false;
		PvpComponent.isPlayingWithBot = false;
		if (client == null) {
			client = new Client(EndPoint);
			client.OnOpen += (sender, e) => {
				print("Connect Success");
				StartJoinRoom();
			};
			client.OnError += (sender, e) => {
				client = null;
				if (e.Message.Contains("Failed to auto-create room"))
					userConnectionErrorType = ConnectionErrorType.ROOM_NOT_FOUND;
				else {
					Debug.LogError(e.Message);
					if (!PvpComponent.isPlayingWithBot && !PvpComponent.userForceDisconnect)
						userConnectionErrorType = ConnectionErrorType.DISCONNECT;
				}
			};
			client.OnClose += (sender, e) => {
				Debug.Log("CONNECTION CLOSED");
				client = null;
				if (!PvpComponent.isPlayingWithBot && !PvpComponent.userForceDisconnect)
					userConnectionErrorType = ConnectionErrorType.DISCONNECT;
			};

			StartCoroutine(client.Connect());
		}
		else {
			StartJoinRoom();
		}
	}
}