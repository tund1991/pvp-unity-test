public enum GameResult
{
    WIN = 0,
    DRAW,
    LOSE
}