public class PvpComponent
{
    public bool isActive = false;
    public PvpPlayer myPlayer;
    public PvpPlayer opponentPlayer;

    public bool isPlaying;
    public bool userForceDisconnect;
    public bool leaveRoomOnFindMatch;
    public bool isPlayingWithBot;
    public int currentBotActionIndex;
    public int currentBotShootIndex;

    public int betValue;

    public int totalObstacleHP;
    public int progressHP;
    public int progressPercent;
    public int conditionValue = 0;

    public int rubyReward = 0;
}