public enum ConnectionErrorType
{
    NORMAL= 0,
    DISCONNECT,
    ROOM_NOT_FOUND,
    SCHEMA_TYPE_MISMATCH
}