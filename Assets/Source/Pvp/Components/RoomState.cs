public enum RoomState
{
    WAITING_FOR_PLAYER = 0,
    WAITING_FOR_MATCH_START,
    PLAYING,
    GAME_OVER
}