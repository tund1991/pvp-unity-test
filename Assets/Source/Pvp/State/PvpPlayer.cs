// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.4.32
// 

using Colyseus.Schema;

public class PvpPlayer : Schema {
	[Type(0, "string")]
	public string id = "";

	[Type(1, "boolean")]
	public bool connected = false;

	[Type(2, "boolean")]
	public bool isWin = false;

	[Type(3, "boolean")]
	public bool isDead = false;

	[Type(4, "int32")]
	public int playDuration = 0;

	[Type(5, "number")]
	public float point = 0;

	[Type(6, "int16")]
	public short currentTurn = 0;

	[Type(7, "array", "int16")]
	public ArraySchema<short> playingMap = new ArraySchema<short>();

	[Type(8, "string")]
	public string name = "";

	[Type(9, "string")]
	public string country = "";

	[Type(10, "int16")]
	public short battlePoint = 0;

	[Type(11, "int16")]
	public short battlePointEnd = 0;

	[Type(12, "int16")]
	public short battlePointWin = 0;

	[Type(13, "int16")]
	public short battlePointLose = 0;

	[Type(14, "int16")]
	public short streak = 0;
	
	[Type(15, "int16")]
	public short win = 0;
	
	[Type(16, "int16")]
	public short lose = 0;
	
	[Type(17, "int16")]
	public short pvpTitle = 0;
	
	[Type(18, "int16")]
	public short pvpRank = 0;
	
	[Type(19, "string")]
	public string ship = "";
	
	[Type(20, "string")]
	public string sub = "";
	
	[Type(21, "string")]
	public string core = "";
	
	[Type(22, "boolean")]
	public bool isFinish = false;
}

