// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 0.4.32
// 

using Colyseus.Schema;

public class PvpState : Schema {
	[Type(0, "map", typeof(MapSchema<PvpPlayer>))]
	public MapSchema<PvpPlayer> players = new MapSchema<PvpPlayer>();

	[Type(1, "int16")]
	public short level = 0;

	[Type(2, "int16")]
	public short roomState = 0;

	[Type(3, "int16")]
	public short gameOverReason = 0;

	[Type(4, "int16")]
	public short moneyBet = 0;

	[Type(5, "int16")]
	public short gameMode = 0;

	[Type(6, "number")]
	public float currentStateStartTime = 0;

	[Type(7, "string")]
	public string winner = "";

	[Type(8, "int32")]
	public int duration = 0;

	[Type(9, "int16")]
	public short startDuration = 0;
}

